module gitlab.com/lsoftop/api-server

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	gopkg.in/ini.v1 v1.47.0
)
